#!/usr/bin/env bash

POLYBAR_VERSION=3.6.3
ALACRITTY_VERSION=0.11.0

main(){
    # main function
#    installMain
#    installBetterlockscreen
#    installRust
#    installAlacritty
#    installPolybar
    #installEndeavour
    installWayland
    installHyprland
}

installEndeavour() {
    yay -Sy git screen nmap htop kitty powertop flatpak xfce4-clipman-plugin xss-lock atril docker docker-compose dialog xdotool signal-desktop postman-bin visual-studio-code-bin zsh ncdu
    yay -Sy python-virtualenvwrapper kora-icon-theme picom eza broot git-delta betterlockscreen i3lock-color inter-font greetd nwg-hello seahorse oh-my-posh
    # dell laptop optionals
    # yay -Sy vulkan-intel evdi-git displaylink tlp
    git clone --bare https://gitlab.com/m.devrees/dotfiles.git "$HOME"/dotfiles
    config() {
        /usr/bin/git --git-dir="$HOME"/dotfiles/ --work-tree="$HOME"
    }
    #alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
    config checkout -f arch
    config config --local status.showUntrackedFiles no
    rm -r "$HOME"/.oh-my-zsh
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    gitClones 
    rm -r "$HOME"/.vim_runtime
    git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
    sh ~/.vim_runtime/install_awesome_vimrc.sh
    mkdir -p "$HOME"/git/github
    git clone https://github.com/linuxdotexe/nordic-wallpapers.git "$HOME"/git/github/nordic-wallpapers
    # xdg-mime default firefox.desktop x-scheme-handler/https x-scheme-handler/http
    # also check https://wiki.archlinux.org/title/GNOME/Keyring for PAM keyring
    # also check https://wiki.archlinux.org/title/Fprint
    afterInstall
}

afterInstall() {
    echo "=== Things to do after install ==="
    echo "
        1. Configure greetd settings for nwg-hello login manager (https://github.com/nwg-piotr/nwg-hello)
        2. Configure pam.d settings 
        2.1 Configure fprint and pam.d settings: https://wiki.archlinux.org/title/Fprint
        2.2 Configure the gnome-keyring for greetd: https://wiki.archlinux.org/title/GNOME/Keyring#Using_the_keyring
        3. Place ~/.local/bin/start-sway in /usr/local/bin/start-sway, this adds env variables
        3.1 Copy /usr/share/wayland-sessions/sway.desktop to /usr/local/share/wayland-sessions/sway.desktop
        3.2 Edit /usr/share/wayland-sessions/sway.desktop to Exec=/usr/local/bin/start-sway
        3.3 If opted for Hyprland, do the same as 3.1 but for Hyprland. Edit the exec to Hyprland -c $HOME/.config/hypr/hyprland-nvidia.conf for desktop
        Leave it default for laptop
    "
}

installWayland() {
    yay -R flameshot && yay -S qt5-tools flameshot-git
    yay -Sy wlroots wayland wofi waybar waylock copyq xorg-xwayland swayidle qt5-wayland imv grim xdg-desktop-portal-wlr xdg-desktop-portal-gtk grim grimshot cliphist kanshi nwg-displays rofi-wayland wl-clipboard qt6ct
}

installHyprland() {
    yay -Sy hyprland hyprlock hypridle hyprcursor xdg-desktop-portal-gtk xdg-desktop-portal-hyprland hyprpicker hyprpaper
}

installFedora(){
    # terminals
    sudo dnf remove i3
    sudo dnf install i3-gaps alacritty polybar xfce4-clipman rofi flameshot autorandr picom arc-theme moka-icon-theme lxappearance screen nmap htop redshift redshift-gtk easyeffects lsp-plugins thunderbird j4-dmenu-desktop

    # docker
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
    sudo dnf install docker-ce docker-ce-cli containerd.io
    sudo usermod -a -G docker $USER

    # docker compose
    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
}

installMain(){
    sudo add-apt-repository ppa:regolith-linux/release
    # moka icons
    sudo add-apt-repository ppa:snwh/ppa
    sudo apt update
    sudo apt install i3 i3-wm i3-gaps scrot xfce4-clipman nitrogen i3lock flameshot autorandr fonts-font-awesome compton arc-theme moka-icon-theme lxappearance screen nmap htop encfs redshift redshift-gtk pulseeffects lsp-plugins kdiff3
}

installPolybar(){
    sudo apt install build-essential git cmake cmake-data pkg-config libcairo2-dev libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev python3-xcbgen xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-dev libuv1-dev python3-sphinx
    mkdir -p Documents/polybar
    cd Documents/polybar && wget https://github.com/polybar/polybar/releases/download/${POLYBAR_VERSION}/polybar-${POLYBAR_VERSION}.tar.gz
    tar xf polybar-${POLYBAR_VERSION}.tar.gz
    cd polybar-${POLYBAR_VERSION} && mkdir build
    cd build && cmake ..
    make -j$(nproc)
    # Optional, this will install the polybar executable in /usr/local/bin
    sudo make install
}

installAlacritty(){
    # prerequisites
    sudo apt install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
    rm -rf ~/Documents/alacritty
    mkdir -p ~/Documents/alacritty
    cd ~/Documents/alacritty && wget https://github.com/alacritty/alacritty/archive/refs/tags/v${ALACRITTY_VERSION}.tar.gz
    tar xf v${ALACRITTY_VERSION}.tar.gz
    cd alacritty-${ALACRITTY_VERSION}
    cargo build --release
    sudo cp target/release/alacritty /usr/local/bin/alacritty
}

installRust(){
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    rustup override set stable
    rustup update stable
}

installBetterlockscreen(){
    # prerequisites
    # Debian / Ubuntu
    #sudo apt install autoconf gcc make pkg-config libpam0g-dev libcairo2-dev libfontconfig1-dev libxcb-composite0-dev libev-dev libx11-xcb-dev libxcb-xkb-dev libxcb-xinerama0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util-dev libxcb-xrm-dev libxcb-xtest0-dev libxkbcommon-dev libxkbcommon-x11-dev libjpeg-dev

    # fedora
    #sudo dnf install -y autoconf automake cairo-devel fontconfig gcc libev-devel libjpeg-turbo-devel libXinerama libxkbcommon-devel libxkbcommon-x11-devel libXrandr pam-devel pkgconf xcb-util-image-devel xcb-util-xrm-devel

    rm -rf ~/Documents/i3lock-color
    mkdir -p ~/Documents/i3lock-color
    cd ~/Documents/i3lock-color && git clone https://github.com/Raymo111/i3lock-color.git
    cd i3lock-color && ./install-i3lock-color.sh
    mkdir -p ~/Documents/betterlockscreen
    cd ~/Documents/betterlockscreen && wget https://github.com/pavanjadhaw/betterlockscreen/archive/refs/heads/main.zip
    unzip main.zip
    cd betterlockscreen-main/ && chmod u+x betterlockscreen
    sudo cp betterlockscreen /usr/local/bin/
    sudo cp system/betterlockscreen@.service /usr/lib/systemd/system/
    sudo systemctl enable betterlockscreen@$USER
}

gitClones(){
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    git clone https://github.com/lukechilds/zsh-nvm ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-nvm
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting 
    mkdir -p ~/.config/alacritty/themes
    git clone https://github.com/alacritty/alacritty-theme ~/.config/alacritty/themes
}

bitwardenCli(){
    wget -qO- https://vault.bitwarden.com/download/?app=cli&platform=linux | zcat >> bw
    sudo install -D -o root -g root -m 755 bw /usr/bin/bw
}
main
