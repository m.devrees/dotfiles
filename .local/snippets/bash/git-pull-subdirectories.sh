#!/usr/bin/env bash

find . -depth -maxdepth 1 -type d -exec git --git-dir={}/.git --work-tree=$PWD/{} pull \;
