
def tree(length):
    for lines in range(1,length):
        if lines % 2 == 1:
            print((('*')*lines).center(length))
    print('*'.center(length))
tree(11) 
