export TERMINAL=alacritty
#export QT_QPA_PLATFORMTHEME=qt5ct


# Added by Toolbox App
export PATH="$PATH:/home/mycha/.local/share/JetBrains/Toolbox/scripts"

if [ -f "$HOME/.cargo/env" ]; then
    . "$HOME/.cargo/env"
fi

# added for sway/wayland
#export MOZ_ENABLE_WAYLAND=1
#export XDG_CURRENT_DESKTOP=sway
#export XDG_SESSION_TYPE=wayland
#export WLR_NO_HARDWARE_CURSORS=1
##export WLR_NO_HARDWARE_CURSORS=0
#export WLR_RENDERER_ALLOW_SOFTWARE=1

# Added by LM Studio CLI (lms)
export PATH="$PATH:/home/mycha/.cache/lm-studio/bin"
