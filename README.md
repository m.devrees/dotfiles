# Dotconfig bare repo
This is a bare repository and should be cloned via `git clone --bare git@gitlab.com:m.devrees/dotfiles.git dotfiles`.
It is inspired by `The best way to store your dotfiles: A bare Git repository` by [Atlassian](https://www.atlassian.com/git/tutorials/dotfiles).

In short, what happens:

```bash
git init --bare $HOME/dotfiles
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no
echo "alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc
echo "alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'" >> $HOME/.zshrc
```

# Installing on new system
Prior to cloning, add the above alias to zshrc or bashrc again. Next, clone the repo
`git clone --bare git@gitlab.com:m.devrees/dotfiles.git $HOME/dotfiles`

Change the config to hide untracked files via `config config --local status.showUntrackedFiles no` and check for changes

# Set touchbar reverse scrolling and tapping
Navigate to `/usr/share/X11/xorg.conf.d/` and edit a file with `libinput.conf` in its name. In the file, search for `touchpad` and add the following options:
```
Option "NaturalScrolling" "True"
Option "tapping" "on"
```

The entire part should look something like
```
Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
        Option "NaturalScrolling" "True"
        Option "tapping" "on"
EndSection
```

# PulseEffects
PulseEffects is a nice interface to mess about with audiosettings in pulseaudio. Right now I've enabled bass boost and set the equalizer to preset gstreamer_ballad.

# What about other Dotfiles?
There's a [separate repository](https://gitlab.com/m.devrees/configuration) which contains configurations regarding other dotfiles, including a README file on how to install those.

