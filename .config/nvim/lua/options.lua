require "nvchad.options"

-- add yours here!

local o = vim.o
local api = vim.api
o.cursorlineopt ='both' -- to enable cursorline!
o.rnu = true -- enable relative line numbers by default
o.colorcolumn = "120"
api.nvim_create_autocmd('TextYankPost', {
  group = vim.api.nvim_create_augroup('highlight_yank', {}),
  desc = 'Hightlight selection on yank',
  pattern = '*',
  callback = function()
    vim.highlight.on_yank { higroup = 'IncSearch', timeout = 500 }
  end,
})
-- api.nvim_set_keymap("n","gd")
