```bash
sudo apt install build-essential git cmake cmake-data pkg-config libcairo2-dev libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev python3-xcbgen xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-dev libjsoncpp-dev
```

Download latest release from https://github.com/jaagr/polybar/releases  

```bash
$ tar xvf polybar-<version>.tar
$ cd polybar/
$ mkdir build
$ cd build/
$ cmake ..
$ make -j$(nproc)
$ sudo make install
```

Start
```bash
$ install -Dm644 /usr/local/share/doc/polybar/config $HOME/.config/polybar/config
$ polybar example
```

i3 config:
```bash
# exec Polybar
exec_always --no-startup-id ~/.config/polybar/launch_polybar.sh &
```
Remove the entire `bar {}` section
