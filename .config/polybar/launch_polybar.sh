#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

BAR_NAME=main
BAR_CONFIG=$HOME/.config/polybar/config.ini

# looks like when logging in to i3, there is no primary display set just yet
# autorandr will trigger primary display
# autorandr --change

if type "xrandr"; then
  # https://github.com/polybar/polybar/issues/1070
  PRIMARY=$(xrandr --query | grep " connected" | grep "primary" | cut -d" " -f1)
  OTHERS=$(xrandr --query | grep " connected" | grep -v "primary" | cut -d" " -f1)

  # Launch on primary monitor
  MONITOR=$PRIMARY polybar --reload -c $BAR_CONFIG $BAR_NAME 2>&1 | tee -a /tmp/polybar.log & disown
  sleep 1

  # Launch on all other monitors
  for m in $OTHERS; do
    MONITOR=$m polybar --reload -c $BAR_CONFIG $BAR_NAME 2>&1 | tee -a /tmp/polybar.log & disown
  done
fi
#else
#  polybar --reload -c $BAR_CONFIG $BAR_NAME &
#fi
