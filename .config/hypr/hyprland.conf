# This is an example Hyprland config file.
#
# Refer to the wiki for more information.

#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#

# See https://wiki.hyprland.org/Configuring/Monitors/
#monitor=eDP-1,1920x1200,0x0,1


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox
# exec-once = waybar &

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf
source = $HOME/.config/hypr/config.d/00-autostart-applications.conf
source = $HOME/.config/hypr/config.d/10-hyprtools.conf
source = $HOME/.config/hypr/config.d/20-env-variables.conf
#source = $HOME/.config/hypr/monitors.conf

# Set programs that you use
$terminal = alacritty
$fileManager = thunar
$menu = rofi -modi drun -show drun -config $HOME/.config/rofi/rofidmenu.rasi
$backlight = $HOME/.config/hypr/scripts/backlight
$notifyvolume = $HOME/.config/hypr/scripts/notify-volume
$exit = $HOME/.config/hypr/scripts/exit

# Some default env vars.
env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORMTHEME,qt6ct # change to qt6ct if you have that

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =
    repeat_rate = 50
    repeat_delay = 200
    follow_mouse = 2
    mouse_refocus = false
    touchpad {
        natural_scroll = true
        scroll_factor = 0.3
        disable_while_typing = false
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 2
    gaps_out = 6
    border_size = 1
    col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
    col.inactive_border = rgba(595959aa)

    layout = master

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 2

    blur {
        enabled = true
        size = 3
        passes = 2
        
        vibrancy = 0.1696
    }

    # drop_shadow = true
    # shadow_range = 4
    # shadow_render_power = 3
    # col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = false

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = false # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = true # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    allow_small_split = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = false
}

misc {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    force_default_wallpaper = -1 # Set to 0 to disable the anime mascot wallpapers
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#per-device-input-configs for more
device {
    name = epic-mouse-v1
    sensitivity = -0.5
}

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
# windowrulev2 = nomaximizerequest, class:.* # You'll probably like this.
windowrulev2 = suppressevent maximize, class:.* # You'll probably like this.
windowrulev2 = float,class:(Toplevel)
windowrulev2 = workspace 2,class:(firefox)
windowrulev2 = workspace 5,class:(Spotify)
windowrulev2 = idleinhibit fullscreen, fullscreen:1

source = $HOME/.config/hypr/config.d/30-windowrules.conf
# place these rules explicitly here
# windowrulev2=windowdance,class:^(jetbrains-.*)$
# search dialog
windowrulev2=dimaround,class:^(jetbrains-.*)$,floating:1,title:^(?!win)
windowrulev2=center,class:^(jetbrains-.*)$,floating:1,title:^(?!win)
# autocomplete & menus
windowrulev2=noanim,class:^(jetbrains-.*)$,title:^(win.*)$
windowrulev2=noinitialfocus,class:^(jetbrains-.*)$,title:^(win.*)$
windowrulev2=rounding 0,class:^(jetbrains-.*)$,title:^(win.*)$

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, $terminal
bind = $mainMod, Q, killactive,
bind = $mainMod SHIFT, E, exec, $exit
bind = $mainMod SHIFT, F, exec, $fileManager
bind = $mainMod SHIFT, SPACE, togglefloating,
bind = $mainMod, D, exec, $menu
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, F, fullscreen, 1
# bind = $mainMod, J, togglesplit, # dwindle

# Move focus with mainMod + arrow keys
bind = $mainMod, h, movefocus, l
bind = $mainMod, l, movefocus, r
bind = $mainMod, k, movefocus, u
bind = $mainMod, j, movefocus, d

# Move active window on the same workspace
bind = $mainMod SHIFT, H, movewindow, l
bind = $mainMod SHIFT, L, movewindow, r
bind = $mainMod SHIFT, K, movewindow, u
bind = $mainMod SHIFT, J, movewindow, d 

# Resize active window
bind = $mainMod ALT, L, resizeactive, 30 0
bind = $mainMod ALT, H, resizeactive, -30 0
bind = $mainMod ALT, K, resizeactive, 0 -30
bind = $mainMod ALT, J, resizeactive, 0 30

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Move active workspace to another monitor
bind = $mainMod CTRL, left, movecurrentworkspacetomonitor, l
bind = $mainMod CTRL, right, movecurrentworkspacetomonitor, r


# brightness and volume control
bind = ,XF86MonBrightnessUp, exec, $backlight inc +5%+,
bind = ,XF86MonBrightnessDown, exec, $backlight dec 5%-

bind = ,XF86AudioRaiseVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ +4% && $notifyvolume
bind = ,XF86AudioLowerVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ -4% && $notifyvolume
bind = ,XF86AudioMute, exec, pactl set-sink-mute @DEFAULT_SINK@ toggle && $notifyvolume
bind = ,XF86AudioMicMute, exec, pactl set-source-mute @DEFAULT_SOURCE@ toggle


# PlayPause command for Spotify
bind = ,XF86AudioPlay, exec, dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
bind = ,XF86AudioNext, exec, dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next
bind = ,XF86AudioPrev, exec, dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous

# Bind clipboard
bind = Alt Ctrl, H, exec, rofi -modi clipboard:$HOME/.config/rofi/cliphist-rofi-img -config $HOME/.config/rofi/rofidmenu.rasi -show clipboard -show-icons 

# Example special workspace (scratchpad)
bind = $mainMod, S, togglespecialworkspace, magic
bind = $mainMod SHIFT, S, movetoworkspace, special:magic

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

bind = ,Print, exec, flameshot gui
bind = $mainMod, Print, exec, flameshot gui --raw | wl-copy

bind = $mainMod, x, exec, loginctl lock-session

# Show waybar on SUPER press and hide on release
#bindit = $mainMod, SUPER_L, exec, pkill -SIGUSR1 waybar
#bindirt = $mainMod, SUPER_L, exec, pkill -SIGUSR1 waybar
#bind = $mainMod, B, exec, pkill -SIGUSR1 waybar

# toggle mute notifications via dunstctl
bind = $mainMod SHIFT, m, exec, dunstctl set-paused toggle
bind = $mainMod SHIFT, n, exec, PAUSED=$(dunstctl is-paused) && dunstify "$PAUSED"

