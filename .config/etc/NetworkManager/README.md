these files belong in /etc/NetworkManager/

after placing run `sudo nmcli general reload` and `sudo systemctl restart NetworkManager`

validate by checking contents of `/etc/resolv.conf`
these should point to 127.0.0.1 (port 53)

do not enable the dnsmasq service as NM handles this

see
- https://wiki.archlinux.org/title/dnsmasq
- https://wiki.archlinux.org/title/NetworkManager
- https://fedoramagazine.org/using-the-networkmanagers-dnsmasq-plugin/


