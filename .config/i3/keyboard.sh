#!/usr/bin/env bash
# This shell script is PUBLIC DOMAIN. You may do whatever you want with it.

TOGGLE=$HOME/.keyboard

if [ ! -e $TOGGLE ]; then
    touch $TOGGLE
    setxkbmap -layout us -variant intl
    notify-send "Keyboard set to intl"
else
    rm $TOGGLE
    setxkbmap -layout us
    notify-send "Keyboard set to default"
fi


# setxkbmap -layout us -variant intl
# setxkbmap -layout us
