#!/usr/bin/env bash

if [ ! -f $HOME/.config/picom/skip-picom ]; then
  picom --config $HOME/.config/picom/picom.conf
fi
