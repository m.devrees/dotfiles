#!/usr/bin/env bash
pulseeffects --gapplication-service
# reset effects
pulseeffects -r
# load music preset
pulseeffects -l music
