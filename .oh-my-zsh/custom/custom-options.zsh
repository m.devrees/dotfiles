unsetopt share_history
export EDITOR=nvim
export WINEARCH=win32
export TERM=xterm-256color

if [ -d $HOME/.local/bin ]; then
    PATH="$PATH:$HOME/.local/bin"
fi

if [ -d $HOME/.local/go/bin ]; then
    PATH="$PATH:$HOME/.local/go/bin"
    export GOPATH=$HOME/go
fi

if [ -d $HOME/.local/adb-fastboot ]; then
    PATH="$HOME/.local/adb-fastboot:$PATH"
fi

if [ -f $HOME/.cargo/env ]; then
    . "$HOME/.cargo/env"
fi


#if command -v atuin > /dev/null; then
#  eval "$(atuin init zsh --disable-up-arrow)"
#fi

if command -v fzf > /dev/null; then
  # Set up fzf key bindings and fuzzy completion
  eval "$(fzf --zsh)"
fi
