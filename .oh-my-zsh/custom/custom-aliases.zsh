# alias ls="ls --color=auto -lrth --group-directories-first"
alias ls="eza -lg --group-directories-first --time-style=long-iso -s=mod"
alias config='git --git-dir=$HOME/dotfiles --work-tree=$HOME'
alias yeet='yay -Rns'
alias cat="bat --style=plain"
alias feh="feh --keep-zoom-vp"
