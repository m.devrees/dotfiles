# install virtualenvwrapper using pip install virtualenvwrapper --user
if [ -f $HOME/.local/bin/virtualenvwrapper.sh ] || [ -f /usr/bin/virtualenvwrapper.sh ]; then
    # Locate the global Python where virtualenvwrapper is installed.
    if [ "$VIRTUALENVWRAPPER_PYTHON" = "" ]; then
        VIRTUALENVWRAPPER_PYTHON="$(command \which python3)"
    fi
    export WORKON_HOME=$HOME/.virtualenvs
    if [ -f /usr/bin/virtualenvwrapper.sh ] && [ ! -e $HOME/.local/bin/virtualenvwrapper.sh ]; then
        ln -s /usr/bin/virtualenvwrapper.sh $HOME/.local/bin/virtualenvwrapper.sh
    fi
    source $HOME/.local/bin/virtualenvwrapper.sh
fi

if [ -d $HOME/.pyenv ]; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PATH:$PYENV_ROOT/bin"
fi
