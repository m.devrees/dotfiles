-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This will hold the configuration.
local config = wezterm.config_builder()

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
config.color_scheme = "Catppuccin Mocha"
config.font = wezterm.font_with_fallback({
	{
		family = "FiraCode Nerd Font Med",
		weight = "Medium",
		harfbuzz_features = { "calt=0", "clig=0", "liga=0" },
	},
})
config.font_size = 10.0
config.freetype_load_target = "Light"
config.freetype_render_target = "HorizontalLcd"
-- disable ligatures
config.hide_tab_bar_if_only_one_tab = true
config.window_padding = {
	left = 0,
	right = 0,
	top = 0,
	bottom = 0,
}
config.window_decorations = "RESIZE"
config.use_fancy_tab_bar = false
-- and finally, return the configuration to wezterm
return config
